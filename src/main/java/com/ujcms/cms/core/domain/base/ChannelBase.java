package com.ujcms.cms.core.domain.base;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

/**
 * This class was generated by MyBatis Generator.
 *
 * @author MyBatis Generator
 */
public class ChannelBase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 数据库表名
     */
    public static final String TABLE_NAME = "channel";

    /**
     * 栏目ID
     */
    @NotNull
    @Schema(description="栏目ID")
    private Integer id = 0;

    /**
     * 站点ID
     */
    @NotNull
    @Schema(description="站点ID")
    private Integer siteId = 0;

    /**
     * 上级栏目ID
     */
    @Nullable
    @Schema(description="上级栏目ID")
    private Integer parentId;

    /**
     * 栏目模型ID
     */
    @NotNull
    @Schema(description="栏目模型ID")
    private Integer channelModelId = 0;

    /**
     * 文章模型ID
     */
    @NotNull
    @Schema(description="文章模型ID")
    private Integer articleModelId = 0;

    /**
     * 名称
     */
    @Length(max = 50)
    @NotNull
    @Schema(description="名称")
    private String name = "";

    /**
     * 别名
     */
    @Length(max = 50)
    @NotNull
    @Schema(description="别名")
    private String alias = "";

    /**
     * 类型(1:常规栏目,2:单页栏目,3:转向链接,4:链接到第一个子栏目)
     */
    @NotNull
    @Schema(description="类型(1:常规栏目,2:单页栏目,3:转向链接,4:链接到第一个子栏目)")
    private Short type = 1;

    /**
     * 文章模板
     */
    @Length(max = 255)
    @Nullable
    @Schema(description="文章模板")
    private String articleTemplate;

    /**
     * 栏目模板
     */
    @Length(max = 255)
    @Nullable
    @Schema(description="栏目模板")
    private String channelTemplate;

    /**
     * 图片
     */
    @Length(max = 255)
    @Nullable
    @Schema(description="图片")
    private String image;

    /**
     * 转向链接地址
     */
    @Length(max = 255)
    @Nullable
    @Schema(description="转向链接地址")
    private String linkUrl;

    /**
     * 流程标识
     */
    @Length(max = 50)
    @Nullable
    @Schema(description="流程标识")
    private String processKey;

    /**
     * 是否新窗口打开
     */
    @NotNull
    @Schema(description="是否新窗口打开")
    private Boolean targetBlank = false;

    /**
     * 是否导航菜单
     */
    @NotNull
    @Schema(description="是否导航菜单")
    private Boolean nav = true;

    /**
     * 层级
     */
    @NotNull
    @Schema(description="层级")
    private Short depth = 1;

    /**
     * 排列顺序
     */
    @NotNull
    @Schema(description="排列顺序")
    private Integer order = 999999;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    @Nullable
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(@Nullable Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getChannelModelId() {
        return channelModelId;
    }

    public void setChannelModelId(Integer channelModelId) {
        this.channelModelId = channelModelId;
    }

    public Integer getArticleModelId() {
        return articleModelId;
    }

    public void setArticleModelId(Integer articleModelId) {
        this.articleModelId = articleModelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    @Nullable
    public String getArticleTemplate() {
        return articleTemplate;
    }

    public void setArticleTemplate(@Nullable String articleTemplate) {
        this.articleTemplate = articleTemplate;
    }

    @Nullable
    public String getChannelTemplate() {
        return channelTemplate;
    }

    public void setChannelTemplate(@Nullable String channelTemplate) {
        this.channelTemplate = channelTemplate;
    }

    @Nullable
    public String getImage() {
        return image;
    }

    public void setImage(@Nullable String image) {
        this.image = image;
    }

    @Nullable
    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(@Nullable String linkUrl) {
        this.linkUrl = linkUrl;
    }

    @Nullable
    public String getProcessKey() {
        return processKey;
    }

    public void setProcessKey(@Nullable String processKey) {
        this.processKey = processKey;
    }

    public Boolean getTargetBlank() {
        return targetBlank;
    }

    public void setTargetBlank(Boolean targetBlank) {
        this.targetBlank = targetBlank;
    }

    public Boolean getNav() {
        return nav;
    }

    public void setNav(Boolean nav) {
        this.nav = nav;
    }

    public Short getDepth() {
        return depth;
    }

    public void setDepth(Short depth) {
        this.depth = depth;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}